module Tests

open Xunit
open Util

[<Theory>]
[<InlineData(0, "0:00")>]
[<InlineData(1, "0:01")>]
[<InlineData(60, "1:00")>]
[<InlineData(61, "1:01")>]
let ``formatMinuteOfDay returns the correct format for various inputs`` (input, expected) =
    let actual = Util.formatMinuteOfDay input
    Assert.Equal(expected, actual)

[<Fact>]
let ``getNextHour returns the next hour for input below 23`` () =
    let input = 22
    let expected = 23
    let actual = Util.getNextHour input
    Assert.Equal(expected, actual)

[<Fact>]
let ``getNextHour returns 0 for input of 23`` () =
    let input = 23
    let expected = 0
    let actual = Util.getNextHour input
    Assert.Equal(expected, actual)
    
[<Fact>]
let ``parseAsInt returns the parsed integer when input is an integer`` () =
    let input = "42"
    let expected = 42
    let actual = Util.parseAsInt input 0
    Assert.Equal(expected, actual)

[<Fact>]
let ``parseAsInt returns the fallback when input is not an integer`` () =
    let input = "abc"
    let expected = 0
    let actual = Util.parseAsInt input 0
    Assert.Equal(expected, actual)